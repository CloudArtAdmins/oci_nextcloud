#!/bin/sh

# See https://docs.linuxserver.io/general/swag#nextcloud-subdomain-reverse-proxy-example for configuration options
source ./.env &&

podman network create proxy-net

podman pod create \
  --network proxy-net \
  --name=nextcloud-pod


podman run --detach \
  --env MYSQL_DATABASE=nextcloud \
  --env MYSQL_USER=nextcloud \
  --env MYSQL_PASSWORD="$MYSQL_PASSWORD" \
  --env MYSQL_ROOT_PASSWORD="$MYSQL_ROOT_PASSWORD" \
  --volume nextcloud-db:/var/lib/mysql \
  --pod=nextcloud-pod \
  --restart on-failure \
  --read-only=true \
  --name nextcloud-db \
  docker.io/library/mariadb:latest

podman run --detach \
  --pod=nextcloud-pod \
  --restart always \
  --read-only=true \
  --name nextcloud-redis \
  docker.io/library/redis:alpine

podman run --detach \
  --env MYSQL_HOST='nextcloud-db' \
  --env MYSQL_DATABASE=nextcloud \
  --env MYSQL_USER=nextcloud \
  --env MYSQL_PASSWORD="$MYSQL_PASSWORD" \
  --env NEXTCLOUD_ADMIN_USER="$NEXTCLOUD_ADMIN_USER" \
  --env NEXTCLOUD_ADMIN_PASSWORD="$NEXTCLOUD_ADMIN_PASSWORD" \
  --env NEXTCLOUD_TRUSTED_DOMAINS="$SUBDOMAIN.$DOMAIN" \
  --env HTTP_X_FORWARDED_HOST="$SUBDOMAIN.$DOMAIN" \
  --env TRUSTED_PROXIES=proxy.dns.podman \
  --env OVERWRITEHOST="$SUBDOMAIN.$DOMAIN" \
  --env OVERWRITEPROTOCOL=https \
  --env REDIS_HOST=nextcloud-redis \
  --env SMTP_HOST=$SMTP_HOST \
  --env SMTP_SECURE=$SMTP_SECURE \
  --env SMTP_PORT=$SMTP_PORT \
  --env SMTP_AUTHTYPE=$SMTP_AUTHTYPE \
  --env SMTP_NAME=$SMTP_NAME \
  --env SMTP_PASSWORD=$SMTP_PASSWORD \
  --env MAIL_FROM_ADDRESS=$MAIL_FROM_ADDRESS \
  --env MAIL_DOMAIN=$MAIL_DOMAIN \
  --volume nextcloud-app:/var/www/html \
  --volume nextcloud-data:/var/www/html/data \
  --pod=nextcloud-pod \
  --read-only=true \
  --restart on-failure \
  --name nextcloud \
  docker.io/library/nextcloud:latest

podman run --detach \
  --name nextcloud-cron \
  --pod=nextcloud-pod \
  --read-only=true \
  --restart always \
  --volume nextcloud-app:/var/www/html \
  --volume nextcloud-data:/var/www/html/data \
  --entrypoint="/cron.sh" \
  docker.io/library/nextcloud:latest

podman run \
  --detach \
  --name=proxy \
  --network proxy-net \
  -e TZ=US/Arizona \
  -e URL="$DOMAIN" \
  -e SUBDOMAINS="$SUBDOMAIN" \
  -e VALIDATION=dns \
  -e DNSPLUGIN=cloudflare \
  -e EMAIL="$LETSENCRYPT_EMAIL" \
  -p 8443:443 \
  -p 8080:80 \
  -v swag-proxy:/config \
  --restart unless-stopped \
  ghcr.io/linuxserver/swag

