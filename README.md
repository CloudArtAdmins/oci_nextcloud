# OCI_Nextcloud

## Quick Start
Clone this repository:  
`git clone https://gitlab.com/PrivateSeabass/oci_nextcloud.git`  

Copy server credentials template:  
`cp .env.template .env`  

Run setup.sh to setup the containers:
`sh setup.sh`

Stop the proxy:
`podman stop proxy`  

Copy a nextcloud config template:
`cp $HOME/.local/share/containers/storage/volumes/swag-proxy/nginx/proxy-conf/nextcloud.subdomain.conf.example $HOME/.local/share/containers/storage/volumes/swag-proxy/nginx/proxy-conf/nextcloud.subdomain.conf`

Edit the following files:
* $HOME/.local/share/containers/storage/volumes/swag-proxy/dns-conf/cloudflare.ini  
* $HOME/.local/share/containers/storage/volumes/swag-proxy/nginx/proxy-conf/nextcloud.subdomain.conf  

Lastly, restart the proxy:
`podman start proxy`
